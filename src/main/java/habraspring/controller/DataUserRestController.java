package habraspring.controller;

import habraspring.exception.NotFoundException;
import habraspring.exception.UserNotFoundException;
import habraspring.model.Data;
import habraspring.model.User;
import habraspring.repository.DataRepository;
import habraspring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
public class DataUserRestController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    DataRepository dataRepository;

//    @RequestMapping(value = "/users", method = RequestMethod.GET)
//    Collection<User> getUsers(@RequestParam String username, @RequestParam String password){
//        authUser(username, password);
//        return this.userRepository.findAll();
//    }

    @RequestMapping(value = "/user")
    User getUser(@RequestParam String username, @RequestParam String password, @RequestParam Long userid){
        authUser(username, password);
        validateUserById(userid);
        return this.userRepository.findOne(userid);
    }

//    @RequestMapping(value = "/datas", method = RequestMethod.GET)
//    Collection<Data> getDatas(@RequestParam String username, @RequestParam String password){
//        authUser(username, password);
//        return this.dataRepository.findAll();
//    }

    @RequestMapping(value = "/data")
    Data getData(@RequestParam String username, @RequestParam String password, @RequestParam Long dataid){
        authUser(username, password);
        validateDataById(dataid);
        return this.dataRepository.findOne(dataid);
    }

    private void authUser(String username, String password) {
        this.userRepository.findByUsernameAndPassword(username, password).orElseThrow(
                UserNotFoundException::new);
    }

    private void validateUserById(Long id) {
        this.userRepository.findById(id).orElseThrow(
                NotFoundException::new);
    }

    private void validateDataById(Long id) {
        this.dataRepository.findById(id).orElseThrow(
                NotFoundException::new);
    }

}
