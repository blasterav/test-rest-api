package habraspring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by blasterav on 11.10.16.
 */
@Entity
public class Data {

    @Id
    private Long id;

    private String title;

    @JsonIgnore
    @ManyToOne
    private User user;

    public Data() {
    }

    public Data(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }


}
