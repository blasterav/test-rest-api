package habraspring.repository;

import habraspring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by blasterav on 11.10.16.
 */
public interface UserRepository extends JpaRepository<User, Long>{
    Optional<User> findById(Long id);
    Optional<User> findByUsernameAndPassword(String username, String password);
}
