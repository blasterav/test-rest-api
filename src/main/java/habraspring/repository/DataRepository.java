package habraspring.repository;

import habraspring.model.Data;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


/**
 * Created by blasterav on 11.10.16.
 */
public interface DataRepository extends JpaRepository<Data, Long>{
    Optional<Data> findById(Long id);
}
